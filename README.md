# The Stucker

## A project by students of VanLang University

- Tran Cong Tu - 187it07007
- Nguyen Phuc Nguyen - 187it06912
- Truong Hoang Long - 187it20962

## Tech used

- HTML/CSS
- PHP
- Bootstrap 4
- Laravel 8.x

## How to use

- Login admin: trancongtu550@gmail.com / pass:Anhem550
- Login user(editor): user@gmail.com / pass: user
- Can order without login and review when order complete
- Go http://laravel.550studios.com/admin to change products, price, customer.
- http://laravel.550studios.com/shop to show all products



